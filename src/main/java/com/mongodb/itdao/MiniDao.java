package com.mongodb.itdao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.google.gson.Gson;

/**
* @author 库琦
* @version 2017年12月24日 下午7:34:10
* 
*/

public class MiniDao {
	private static final String _uuid="_uuid";
	/**
	 * 查询数据
	 * @throws IOException 
	 * @throws JDOMException 
	 * 
	 * */
	public List<Map> loadTableDatas(String path,String tablename) throws JDOMException, IOException{
		           Gson gson = new Gson();
		           FileInputStream file = new FileInputStream(path);
		           SAXBuilder saxbuilder = new SAXBuilder();
		           Document document = saxbuilder.build(file);
		           Element root = document.getRootElement();
		          List<Map> list = new ArrayList<Map>();
		            List<Element> children = root.getChildren();
		          for(Element x:children){
		        	  if(x.getAttributeValue("name").equals(tablename)){
		        		  List<Element> al = x.getChildren();
		        		  for(Element s:al){
		        			  String data = s.getText();
		        			  if(data == null){
		        				  break;
		        			  }
		        			  Map json = gson.fromJson(data, Map.class);
		        			  list.add(json);
		        		  }
		        		  
		        	  }
		          }
				return list;
		           
		
		
		
		
		
	}
	/**
	 * 删除信息。
	 * @throws IOException 
	 * @throws JDOMException 
	 * 
	 * */
	public void deleteData(String path,String tablename,Object  obj ) throws JDOMException, IOException{
		        FileInputStream file = new FileInputStream(path);
		                          Gson gson = new Gson();
		           SAXBuilder builder = new SAXBuilder();
		           Document document = builder.build(file);
		           Element root = document.getRootElement();
		           List<Element> child = root.getChildren();
		          for(Element x:child){
		        	  if(x.getAttributeValue("name").equals(tablename)){
		        		  List<Element> al = x.getChildren();
		        		  for(Element s:al){
		        			   Map mp = gson.fromJson(s.getText(), Map.class);
		        				Map newmp = gson.fromJson(gson.toJson(obj), Map.class);
		        			   if(mp.get(_uuid).equals(newmp.get(_uuid))){
		 		            	  x.removeContent(s);
		 		            	  
		 		            	  break;
		 		            	   
		 		               }
		        		  }
		        		  
		        	  }
		        	  
		        	  
		        	  
		          }
		      	XMLOutputter out = new XMLOutputter();
				out.output(document, new FileOutputStream(path));
		           
		
	}
	
	/**
	 * 
	 * 
	 * 表插入数据。
	 * @throws IOException 
	 * @throws JDOMException 
	 * */
	@SuppressWarnings("rawtypes")
	public boolean addData(String path,String tablename,Object po) throws JDOMException, IOException{
		   Gson gson = new Gson();
		   boolean flag = false;
		   FileInputStream file = new FileInputStream(path);
		      SAXBuilder builder = new SAXBuilder();
		      Document document = builder.build(file);
		      Element root = document.getRootElement();
		       List<Element> list = root.getChildren();
		       for(Element x:list){
		     if(x.getAttributeValue("name").equals(tablename)){
		       Map base = new HashMap();
			   base.put(_uuid, UUID.randomUUID().toString().replaceAll("-", ""));
			   String json = gson.toJson(po);
			     Element data = new Element("data");
			    Map mp = gson.fromJson(json, Map.class);
			    base.putAll(mp);
			    data.addContent(gson.toJson(base));
			    x.addContent(data);
			    }
			    }
		          XMLOutputter out = new XMLOutputter();
		          out.output(document, new FileOutputStream(path));
		          flag = true;
		return flag;
		
		
		
	}
	public void updateData(String path,String tablename,Object po) throws JDOMException, IOException{
		         FileInputStream file = new FileInputStream(path);
		         SAXBuilder saxBuilder = new SAXBuilder();
		         Document document = saxBuilder.build(file);
		         Element root = document.getRootElement();
		              Gson gson = new Gson();
		         List<Element> list = root.getChildren();
		        for(Element x:list){
		        	if(x.getAttributeValue("name").equals(tablename)){
		        		   List<Element> al = x.getChildren();
		        		for(Element s:al){
		        	   Map mp = gson.fromJson(s.getText(), Map.class);
		               Map newMap = gson.fromJson(gson.toJson(po), Map.class);
		               if(mp.get(_uuid).equals(newMap.get(_uuid))){
		            	  mp.putAll(newMap); 
		            	  Element data= new Element("data");
		            	  data.setText(gson.toJson(mp));
		            	  x.removeContent(s);
		            	  x.addContent(data);
		            	  break;
		            	   
		               }
		        			
		        	}
		         }
		        	
		      }
		          XMLOutputter out = new XMLOutputter();
		          out.output(document, new FileOutputStream(path));
	}
	
	
	
	
	
	public void createData(String path,String baseData) throws Exception{
		FileOutputStream file1 = new FileOutputStream(path);
		Document document = new Document();
		Element root = new Element("database");
		Element sort1 = new Element("table");
		sort1.setAttribute("name", baseData);
	
		root.addContent(sort1);
		


		document.setRootElement(root);
		XMLOutputter out = new XMLOutputter();
		out.output(document, file1);
	
	}
  
	
		
		
		
	
	
	

}
