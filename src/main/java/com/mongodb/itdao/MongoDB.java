package com.mongodb.itdao;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;

/**
* @author 库琦
* @version 2017年12月18日 下午4:18:31
* 
*/

public class MongoDB {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
	
		Mongo mongo = new Mongo("127.0.0.1", 27017); 
		 for(String name : mongo.getDatabaseNames()) {

			 System.out.println("dbName: " + name);

			 } 
		 DB db = mongo.getDB("foobar"); 
		 for(String name : db.getCollectionNames()) {

			 System.out.println("collectionName: " + name);

			 } 
		      DBCollection person = db.getCollection("books");
		    
		      
		      DBCursor cur = person.find();
		      while(cur.hasNext()) {

		    	  System.out.println(cur.next());

		    	  } 
		      person.remove(new BasicDBObject("name",1.0));
		      System.out.println("cur.count(): " + cur.count()); 
		      
		      BasicDBObject person1 = new BasicDBObject();
		      person1.put("name", "小学生");
		      person1.put("age", 55);
		      person.save(person1);
		      System.out.println(cur.count());
	}

}
